#include "HomeWork_16.h"

HomeWork_16::HomeWork_16(int size_of_array) : x(size_of_array)
{
	CreateArray();
	FillArray();
}

HomeWork_16::~HomeWork_16()
{
	for (auto i = 0; i < x; ++i)
	{
		delete[] array[i];
	}
}

void HomeWork_16::CreateArray()
{
	array = new int* [x];
	for (auto i = 0; i < x; ++i)
	{
		array[i] = new int[x];
	}
}

void HomeWork_16::FillArray()
{
	for (auto i = 0; i < x; ++i)
	{
		for (auto j = 0; j < x; ++j)
		{
			array[i][j] = i + j;
		}
	}
}

void HomeWork_16::PrintArray()
{
	for (auto i = 0; i < x; ++i)
	{
		for (auto j = 0; j < x; ++j)
		{
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

void HomeWork_16::PrintLineSum()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int index = buf.tm_mday % x;
	int sum = 0;

	for (auto i = 0; i < x; ++i)
	{
		sum += array[index][i];
	}

	std::cout << "Sum of " << index << " line: " << sum << std::endl;
}
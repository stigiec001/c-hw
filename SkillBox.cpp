﻿// SkillBox.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <string>

#include "HomeWork_13.h"
#include "HomeWork_15.h"
#include "HomeWork_16.h"
#include "HomeWork_17.h"
#include "HomeWork_18.h"
#include "HomeWork_19.h"


int main()
{
    /*HomeWork_13 module start*/
    std::cout << "############ " << "Module 13 HW" << " ############" << std::endl;

    std::cout << HomeWork_13::sqrt(1, 2) << std::endl;

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_13 module end*/



    /*HomeWork_14 module start*/
    std::cout << "############ " << "Module 14 HW" << " ############" << std::endl;

    std::string str = "Hello Skillbox!";
    std::cout << str << std::endl;

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_14 module end*/



    /*HomeWork_15 module start*/
    std::cout << "############ " << "Module 15 HW" << " ############" << std::endl;

    HomeWork_15::PrintOddNumbers(10, true);

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_15 module end*/



    /*HomeWork_16 module start*/
    std::cout << "############ " << "Module 16 HW" << " ############" << std::endl;

    HomeWork_16 array(5);
    array.PrintArray();
    array.PrintLineSum();

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_16 module end*/



    /*HomeWork_17 module start*/

    std::cout << "############ " << "Module 17 HW" << " ############" << std::endl;
    HomeWork_17 vector(5, 1, 6);
    vector.Print();
    std::cout << vector.GetLength() << std::endl;

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_17 module end*/



    /*HomeWork_18 module start*/
    std::cout << "############ " << "Module 18 HW" << " ############" << std::endl;

    Stack<std::string> stack;
    std::cout << "Stack size: " << stack.GetSize() << std::endl;
    for (int i = 0; i < 30; ++i)
    {
        stack.Push(std::to_string(i) + " sheep");
    }
    std::cout << "Stack filled!" << std::endl;
    std::cout << "Stack size: " << stack.GetSize() << std::endl;
    std::cout << "Top element: ";
    stack.Peek();
    for (auto i = 0; i < 30; ++i)
    {
        std::cout << stack.Pop() << std::endl;
    }
    std::cout << "Stack size: " << stack.GetSize() << std::endl;

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_18 module end*/



    /*HomeWork_19 module start*/
    std::cout << "############ " << "Module 19 HW" << " ############" << std::endl;

    Animal* animals[3];
    animals[0] = new Cat;
    animals[1] = new Dog;
    animals[2] = new Cow;
    for (auto i : animals)
    {
        i->voice();
    }

    std::cout << "######################################" << std::endl << std::endl;
    /*HomeWork_19 module end*/
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

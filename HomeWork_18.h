#pragma once
#include <assert.h>
#include <iostream>

#define STACK_SIZE 10 
template <typename T>
class Stack
{
private:
	int top = -1;
	int size = STACK_SIZE;
	T* array;

	void Resize(float size_multiplier);
	void Copy(T* from, T* to);

public:
	Stack()  { array = new T[size]; };
	~Stack() { delete[] array; };

	void Push(T element);
	T Pop();
	void Peek();
	int GetSize();

};

template<typename T>
void Stack<T>::Resize(float size_multiplier)
{
	size *= size_multiplier;
	T* temp = new T[size];
	Copy(array, temp);
	delete[] array;
	array = new T[size];
	Copy(temp, array);
	delete[] temp;
}

template<typename T>
void Stack<T>::Copy(T* from, T* to)
{
	for (int i = 0; i <= top; ++i)
	{
		to[i] = from[i];
	}
}

template<typename T>
void Stack<T>::Push(T element)
{
	top++;
	if (top >= size - 1)
		Resize(2);
	array[top] = element;
}

template<typename T>
T Stack<T>::Pop()
{
	assert(top >= 0);
	if (top < size / 2 && size > STACK_SIZE)
		Resize(0.5f);
	return array[top--];
}

template<typename T>
void Stack<T>::Peek()
{
	std::cout << array[top] << std::endl;
}

template<typename T>
int Stack<T>::GetSize()
{
	return size;
}


// 1 2 3 4 5 6 7 8 9 10 size
// 0 1 2 3 4 5 6 7 8 9  top
//
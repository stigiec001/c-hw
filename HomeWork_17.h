#pragma once
#include <iostream>
#include <math.h>

class HomeWork_17
{
	int x;
	int y;
	int z;

public:
	HomeWork_17(int inX, int inY, int inZ) : x(inX), y(inY), z(inZ) {};
	~HomeWork_17() {};

	void Print();
	float GetLength();
};


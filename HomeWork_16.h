#pragma once
#include <iostream>
#include <time.h>

class HomeWork_16
{
	int x = 0;
	int** array;

	void CreateArray();
	void FillArray();

public:
	HomeWork_16(int size_x);
	~HomeWork_16();

	void PrintArray();
	void PrintLineSum();

};


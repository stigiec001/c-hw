#include "HomeWork_17.h"

void HomeWork_17::Print()
{
	std::cout << "x: " << x << "; y: " << y << "; z: " << z << ";" << std::endl;
}

float HomeWork_17::GetLength()
{
	int sum = x * x + y * y + z * z;
	return std::sqrt(sum);
}
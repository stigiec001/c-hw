#pragma once
#include <iostream>

class Animal
{
public:
	Animal() {};
	~Animal() {};

	virtual void voice() {} ;
};

class Cat : virtual public Animal
{
public:
	void voice() override
	{
		std::cout << "Mew!" << std::endl;
	}
};

class Dog : virtual public Animal
{
public:
	void voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cow : virtual public Animal
{
public:
	void voice() override
	{
		std::cout << "Mooo!" << std::endl;
	}
};
